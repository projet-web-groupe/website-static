import React, {Component} from 'react';
import {Link} from "react-router-dom";

export default class SideBarLeft extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <nav class="d-none d-md-block fixed-top" id="sidebar">
                <div class="sidebar-header">
                    <Link to="/"><img class="dim" src="https://bdecesilyon.fr/static/img/logo.png"
                                      alt=""/></Link><br></br>
                </div>

                <ul class="list-unstyled components">
                    <li>
                        <Link to="/manifestations" class="border-bottom">Manifestations </Link><br/>
                    </li>
                    <li>
                        <Link to="/post-manifestation" class="border-bottom">Poster une manifestation </Link><br/>
                    </li>
                    <li>
                        <Link to="/store" class="border-bottom">Boutique</Link><br/>
                    </li>
                    <li>
                        <Link to="/admin" class="border-bottom">Admin</Link>
                    </li>
                </ul>
                <div class="container" class="mt-5 pt-4">
                    <div class="row" class="pt-3 mt-3">
                        <div class=" col-12 text-center">
                            <p>Retrouvez-nous sur</p>
                            <a href="https://facebook.com" target="_blank" class="social-icons"><i class="fab fa-facebook fa-3x"/> </a>
                            <a href="https://twitter.com" target="_blank" class="social-icons"><i class="fab fa-twitter-square fa-3x"/></a>
                            <a href="https://instagram.com" target="_blank" class="social-icons"><i class="fab fa-instagram fa-3x"/></a>
                        </div>
                    </div>
                </div>
            </nav>

        );

    }
}