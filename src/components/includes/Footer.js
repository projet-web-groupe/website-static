import React, { Component } from 'react';

export default class Footer extends Component {
    constructor(props) {
        super(props);

    }S
    render() {
        return (
            <footer class="site-footer sticky-bottom">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-12 col-sm-3 text-center ">
                            <ul>
                                <li><a href="https://www.facebook.com/BDECesiLyon/" class="containers" target="_blank"> Facebook </a></li>
                                <li><a href="https://bdecesilyon.fr/media/documents/STATUTS_DU_BDE_DU_CENTRE_CESI.pdf" target="_blank"> Status du BDE</a></li>
                                <li><a href="https://www.instagram.com/bdscesi/?hl=fr" class="containers" target="_blank"> BDS CESI Lyon </a></li>
                            </ul>
                        </div>
                        <div class=" col-12 col-sm-1 text-center">
                            <ul>
                                <li><a href="CGU.pdf" class="containers" target="_blank"> CGU </a></li>
                                <li><img class="icon-website" src="https://bdecesilyon.fr/static/img/logo.png" alt="" /></li>
                                <li><a href="#" class="containers" target="_blank"> CGV </a></li>
                            </ul>
                        </div>
                        <div class=" col-12 col-sm-3 text-center">
                            <ul>
                                <li><a href="Legal-notice.pdf" class="legalNotice" target="_blank"> Mentions légales </a></li>
                                <li><a href="#" class="credit" target="_blank"> Crédits</a></li>
                                <li><a href="politique-de-confidentialité.pdf" class="privacy policy" target="_blank"> Politique de confidentialité</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}