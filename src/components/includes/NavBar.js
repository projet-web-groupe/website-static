import React, { Component } from 'react';
import { Link } from "react-router-dom";

export default class NavBar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div id="navbar">
                <nav class="navbar navbar-expand-md colordark fixed-top">
                    <div class="container-fluid ">
                        <img class="nav-brand" href="https://bdecesilyon.fr/static/img/logo.png"></img>
                        <button class="navbar-toggler bg-black m-2" type="button" data-toggle="collapse"
                            data-target="#navbarColor02"
                            aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="dark-blue-text"><i class="fas fa-bars fa-1x"></i></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarColor02">
                            <ul class="navbar-nav ml-auto">
                                <li>

                                    <Link to="/"><img class="float-right d-print-block dim-little d-block d-sm-none mr-2" src="https://bdecesilyon.fr/static/img/logo.png" alt="" /></Link>
                                </li>
                                <li>
                                    <Link to="/basket" class="nav-link white-icon float-right"><i class="fas fa-shopping-basket fa-lg mr-3"></i></Link>
                                </li>

                                <li class="nav-item active text-light text-right d-block d-md-none">
                                    <Link to="/manifestations" class="nav-link text-uppercase">Manifestations </Link>
                                </li>


                                <li class="nav-item active text-light text-right d-block d-md-none">
                                    <Link to="/post-manifestation" class="nav-link text-uppercase">Poster manifestation</Link>
                                </li>
                                <li class="nav-item active text-light text-right d-block d-md-none">
                                    <a class="nav-link text-uppercase" href="#">Boutique</a>
                                </li>
                                <li class="nav-item active text-light text-right">
                                    <Link class="nav-link " to="/account"><span>MON COMPTE</span></Link>
                                </li>
                                <li class="nav-item active text-light text-right">
                                    <Link to="/inscription" class="nav-link text-uppercase">Inscription</Link>
                                </li>
                                <li class="nav-item active text-light text-right">
                                    <Link to="/connexion" class="nav-link text-uppercase ">Connexion</Link>
                                </li>

                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            
        );
        
    }
    
}

