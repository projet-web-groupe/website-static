import React, {Component} from 'react';
import {Link} from "react-router-dom";

export default class Template extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div className="banner">
                    <img className="d-block w-100" src="banner-admin.jpg" alt="First slide"/>
                    <div className="title-centered">Espace admin</div>
                </div>
                <div className="container" id="admin">

                    <div className="row justify-content-center">
                        <div className="col-12 text-center my-5">
                            <Link to="/user-management">
                                <button type='submit' className=" m-3 btn btn-secondary btn-lg button-admin">Gérer les
                                    utilisateurs
                                </button>
                            </Link>
                            <Link to="/activity-management">
                                <button type='submit' className=" m-3 btn btn-secondary btn-lg button-admin">Gérer les
                                    manifestations
                                </button>
                            </Link>
                            <Link to="/archive">
                                <button type='submit'
                                        className=" m-3 btn btn-secondary btn-lg button-admin">Archives
                                </button>
                            </Link>
                            <Link to="/store-management">
                                <button type='submit' className=" m-3 btn btn-secondary btn-lg button-admin">Gérer la
                                    boutique
                                </button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}