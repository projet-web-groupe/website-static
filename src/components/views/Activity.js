import React, { Component } from 'react';
import { Link } from "react-router-dom";


export default class Template extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div class="container-activity" id="activity">
                <h3 class="title">Manifestations à la une : </h3>
                <div class="col-lg">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"/>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"/>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"/>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="http://placehold.it/1920x460" alt="First slide" />
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="http://placehold.it/1920x460" alt="Second slide" />
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="http://placehold.it/1920x460" alt="Third slide" />
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"/>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"/>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>

                <h3 class="title"><br></br>Manifestations à venir : </h3>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-sm space">
                            <div class="card">
                                <img src="https://via.placeholder.com/140x100" class="card-img-top" alt="..." />
                                <div class="card-body">
                                    <h5 class="card-title">Manifestation name</h5>
                                    <p class="card-text">Description de la manifestation.</p>
                                    <Link to="/single-activity" class="col text-center space btn btn-primary">Voir plus</Link>
                                </div>
                            </div>
                        </div>
                        <div class=" col-lg-4 col-sm space">
                            <div class="card">
                                <img src="https://via.placeholder.com/140x100" class="card-img-top" alt="..." />
                                <div class="card-body">
                                    <h5 class="card-title">Manifestation name</h5>
                                    <p class="card-text">Description de la manifestation.</p>
                                    <Link to="/single-activity" class="col text-center space btn btn-primary">Voir plus</Link>
                                </div>
                            </div>
                        </div>
                        <div class=" col-lg-4 col-sm space">
                            <div class="card">
                                <img src="https://via.placeholder.com/140x100" class="card-img-top" alt="..." />
                                <div class="card-body">
                                    <h5 class="card-title">Manifestation name</h5>
                                    <p class="card-text">Description de la manifestation.</p>
                                    <Link to="/single-activity" class="col text-center space btn btn-primary">Voir plus</Link>
                                </div>
                            </div>
                        </div>
                        <div class=" col-lg-4 col-sm space">
                            <div class="card">
                                <img src="https://via.placeholder.com/140x100" class="card-img-top" alt="..." />
                                <div class="card-body">
                                    <h5 class="card-title">Manifestation name</h5>
                                    <p class="card-text">Description de la manifestation.</p>
                                    <Link to="/single-activity" class="col text-center space btn btn-primary">Voir plus</Link>
                                </div>
                            </div>
                        </div>
                        <div class=" col-lg-4 col-sm space">
                            <div class="card">
                                <img src="https://via.placeholder.com/140x100" class="card-img-top" alt="..." />
                                <div class="card-body">
                                    <h5 class="card-title">Manifestation name</h5>
                                    <p class="card-text">Description de la manifestation.</p>
                                    <Link to="/single-activity" class="col text-center space btn btn-primary">Voir plus</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}