import React, {Component} from 'react';
import {Link} from "react-router-dom";

export default class Login extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="container" id="login">
                <div className="row my-3">
                    <div className="col-12 text-center">
                        <h1>Connexion</h1>
                    </div>
                </div>

                <div className="row justify-content-center">
                    <div className="col-6">
                        <form action="" method="post">
                            <div className="form-row">
                                <div className="form-group col-12">
                                    <label htmlFor="formGroupExampleInput">Email</label>
                                    <input type="email" className="form-control" id="formGroupExampleInput"
                                           placeholder="Email"/>
                                </div>
                                <div className="form-group col-12">
                                    <label htmlFor="formGroupExampleInput">Mot de passe</label>
                                    <input type="password" className="form-control" id="formGroupExampleInput"
                                           placeholder="******"/>
                                </div>
                                <Link to="password" className="extra-login-register"><b>Mot de passe oublié ?</b></Link>
                            </div>

                            <div className="col-12 text-center my-3">
                                <button type='submit' className="btn btn-primary btn-lg">Se connecter</button>
                            </div>
                        </form>
                        <div>
                            <b>Vous n'avez pas encore d'espace personnel ? <br/>
                                <Link to="/inscription" className="extra-login-register">Inscrivez-vous !</Link></b>
                        </div>

                    </div>
                </div>

            </div>
        )
    }
}