import React, {Component} from 'react';


export default class Template extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div className="banner">
                    <img className="d-block w-100" src="banner-admin.jpg" alt="First slide"/>
                    <div className="title-centered">Espace admin</div>
                    <div className="subtitle-centered">Liste des utilisateur</div>
                </div>
                <div className="container-fluid pt-5 pl-5" id="user-management">
                    <div className="row">
                        <div className="col-12 " className="management">
                            Nom
                        </div>
                        <div className="col-12 " className="management">
                            E-mail
                        </div>
                        <div className="col-12 " className="management">
                            N° de téléphone
                        </div>
                        <div className="col-12 " className="management">
                            Adresse
                        </div>
                        <div className="col-12 " className="management">
                            Localisation
                        </div>
                        <div className="col-12 " className="management">
                            Status
                        </div>
                        <div className="col-3 ">
                            <button type='submit' className="btn btn-primary btn-lg management">Modifier</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}