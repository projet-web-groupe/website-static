import React, { Component } from 'react';
import { Link } from "react-router-dom";


export default class HomePage extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div class="container-fluid " id="homepage">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"/>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"/>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"/>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="http://dev.meilleures-licences.com/photo_ecole/photoeicesi-1448636468.jpg" class="d-block w-100" alt="..." />
                        </div>
                        <div class="carousel-item">
                            <img src="https://lyon.cesi.fr/wp-content/uploads/sites/30/2018/11/CESI_Campus_JPO-1312x711-2.png" class="d-block w-100" alt="..." />
                        </div>
                        <div class="carousel-item">
                            <img src="https://www.ecully.fr/fileadmin/_processed_/e/5/csm_Campus-CESI_7f307fe881.jpg" class="d-block w-100" alt="..." />
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"/>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"/>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <div><br/>
                    <h2>Événements du mois :</h2>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4 col-sm space">
                                <div class="card">
                                    <img src="https://via.placeholder.com/140x100" class="card-img-top" alt="..." />
                                    <div class="card-body">
                                        <h5 class="card-title">Manifestation name</h5>
                                        <p class="card-text">Description de la manifestation.</p>
                                        <Link to="/single-activity" class="col text-center space btn btn-primary">Voir plus</Link>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-lg-4 col-sm space">
                                <div class="card">
                                    <img src="https://via.placeholder.com/140x100" class="card-img-top" alt="..." />
                                    <div class="card-body">
                                        <h5 class="card-title">Manifestation name</h5>
                                        <p class="card-text">Description de la manifestation.</p>
                                        <Link to="/single-activity" class="col text-center space btn btn-primary">Voir plus</Link>
                                    </div>
                                </div>
                            </div>
                            <div class=" col-lg-4 col-sm space">
                                <div class="card">
                                    <img src="https://via.placeholder.com/140x100" class="card-img-top" alt="..." />
                                    <div class="card-body">
                                        <h5 class="card-title">Manifestation name</h5>
                                        <p class="card-text">Description de la manifestation.</p>
                                        <Link to="/single-activity" class="col text-center space btn btn-primary">Voir plus</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div><br></br>
                    <h2>Découvrir le CESI lyon :</h2>
                    <div>
                       <a href="https://lyon.cesi.fr/" target="_blank"><img src="https://www.cesi.fr/wp-content/uploads/2018/11/logo-CESI.png" class="d-block w-100"/></a>
                    </div>
                </div>
            </div>


        );
    }
}