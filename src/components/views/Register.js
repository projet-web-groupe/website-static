import React, { Component } from 'react';
import { Link } from "react-router-dom";


export default class Register extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="container mb-5">
                <div className="row my-3">
                    <div className="col-12 text-center">
                        <h1>Inscription</h1>
                    </div>
                </div>


                <div className="row justify-content-center">
                    <div className="col-6">

                        <form action="" method="post">
                            <div className="form-row">
                                <div className="form-group col-12">
                                    <label htmlFor="formGroupExampleInput">Email</label>
                                    <input type="email" className="form-control" id="formGroupExampleInput"
                                        placeholder="Email" />
                                </div>

                                <div className="form-group col-12">
                                    <label htmlFor="formGroupExampleInput">Prénom</label>
                                    <input type="text" className="form-control" id="formGroupExampleInput"
                                        placeholder="Jean" />
                                </div>

                                <div className="form-group col-12">
                                    <label htmlFor="formGroupExampleInput">Nom</label>
                                    <input type="text" className="form-control" id="formGroupExampleInput"
                                        placeholder="Bon" />
                                </div>

                            </div>

                            <div className="form-row">
                                <div className="form-group col-12 col-sm-6">
                                    <label htmlFor="formGroupExampleInput">Mot de passe</label>
                                    <input type="password" className="form-control" id="formGroupExampleInput"
                                        placeholder="******" />
                                </div>

                                <div className="form-group col-12 col-sm-6">
                                    <label htmlFor="formGroupExampleInput">Confirmation du mot de passe</label>
                                    <input type="password" className="form-control" id="formGroupExampleInput"
                                        placeholder="******" />

                                </div>
                            </div>

                            <div className="col-12 text-center my-3">
                                <button type='submit' className="btn btn-primary btn-lg">S'inscrire</button>
                            </div>
                        </form>
                        <div>
                            <b>Vous n'avez pas encore d'espace personnel ? <br></br>
                                <Link to="/connexion" className="extra-login-register">Connectez-vous !</Link></b>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}