import React, {Component} from 'react';


export default class Template extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div className="banner">
                    <img className="d-block w-100" src="banner-admin.jpg" alt="First slide"/>
                    <div className="title-centered">Espace admin</div>
                    <div className="subtitle-centered">Liste des articles en vente</div>
                </div>
                <div className="container-fluid pt-5 pl-4" id="store-management">
                    <div className="row">
                        <div className="col-12 " className="management">
                            Nom de l'article
                        </div>
                        <div className="col-12 " className="management">
                            quantité
                        </div>
                        <div className="col-12 " className="management">
                            Prix de vente
                        </div>
                        <div className="col-12 " className="management">
                            Date de mise en vente
                        </div>
                        <div className="col-12 " className="management">
                            Nombre d'articles vendu
                        </div>
                        <div className="col-3 ">
                            <button type='submit' className="btn btn-primary btn-lg management">Modifier</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}