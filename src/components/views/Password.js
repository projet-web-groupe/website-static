import React, { Component } from 'react';
import { Link } from "react-router-dom";

export default class Template extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="container" id="password">
                <div className="row my-3">
                    <div className="col-12 text-center">
                        <h1>Veuillez renseigner votre adresse mail :</h1>
                    </div>
                </div>

                <div className="row justify-content-center">
                    <div className="col-6">
                        <div className="form-row form-group col-12">
                            <input type="email" className="form-control" id="formGroupExampleInput"
                                placeholder="Email" />
                        </div>

                        <div className="col-12 text-center my-3">
                            <button type='submit' className="btn btn-primary btn-lg"><Link to="confirmation-password">Envoyer</Link></button>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}