import React, {Component} from 'react';


export default class Template extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div className="banner">
                    <img className="d-block w-100" src="banner-admin.jpg" alt="First slide"/>
                    <div className="title-centered">Espace admin</div>
                    <div className="subtitle-centered">Archive</div>
                </div>
                <div className="container-fluid pt-5 pl-5" id="archive">
                    <div className="row">
                        <div className="col-12 " className="management">
                            Nom Manifestation
                        </div>
                        <div className="col-12 " className="management">
                            Créateur de la manifestation
                        </div>
                        <div className="col-12 " className="management">
                            Date de création
                        </div>
                        <div className="col-12 " className="management">
                            Date de la manifestation
                        </div>
                        <div className="col-3 ">
                            <button type='submit' className="btn btn-primary btn-lg management">Modifier</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}