import React, { Component } from 'react';


export default class Store extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div class="container" id="store">
                <h3 class="title">Article les plus commandés:</h3>
                <div class="col-lg">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="http://placehold.it/1920x460" alt="First slide" />
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="http://placehold.it/1920x460" alt="Second slide" />
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="http://placehold.it/1920x460" alt="Third slide" />
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col text-center space">
                            <button type="button" class="btn btn-primary" data-toggle="button" aria-pressed="false" autocomplete="off">
                                Prix croissant
</button>
                        </div>
                        <div class="col text-center space">
                            <button type="button" class="btn btn-primary" data-toggle="button" aria-pressed="false" autocomplete="off">
                                Prix décroissant
</button>
                        </div>
                        <div class="col text-center space"><button type="button" class="btn btn-primary" data-toggle="button" aria-pressed="false" autocomplete="off">
                            Nom A à Z
</button>
                        </div>
                        <div class="col text-center space"><button type="button" class="btn btn-primary" data-toggle="button" aria-pressed="false" autocomplete="off">
                            Nom Z à A
</button></div>
                    </div>
                    <div class="row">
                        <div class="col-8"></div>
                        <div class="col-4"></div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm space">
                            <div class="card">
                                <img src="http://placehold.it/200x250" class="card-img-top" alt="..." />
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm space">
                            <div class="card">
                                <img src="http://placehold.it/200x250" class="card-img-top" alt="..." />
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm space">
                            <div class="card">
                                <img src="http://placehold.it/200x250" class="card-img-top" alt="..." />
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm space">
                            <div class="card">
                                <img src="http://placehold.it/200x250" class="card-img-top" alt="..." />
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm space">
                            <div class="card">
                                <img src="http://placehold.it/200x250" class="card-img-top" alt="..." />
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm space">
                            <div class="card">
                                <img src="http://placehold.it/200x250" class="card-img-top" alt="..." />
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm space">
                            <div class="card">
                                <img src="http://placehold.it/200x250" class="card-img-top" alt="..." />
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm space">
                            <div class="card">
                                <img src="http://placehold.it/200x250" class="card-img-top" alt="..." />
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm space">
                            <div class="card">
                                <img src="http://placehold.it/200x250" class="card-img-top" alt="..." />
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                    <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}