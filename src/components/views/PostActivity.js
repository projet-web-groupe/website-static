import React, { Component } from 'react';


export default class PostActivity extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div class="container-fluid mb-4 border">
                <div class="row justify-content-center " >
                    <div class="col-lg  text-center mt-2 ">
                        <i class="fas fa-hourglass-start fa-10x"></i>
                        <h1>
                            <u>
                                Crée ta manifestion dès maintenant !
                        </u>
                        </h1>
                        <form class="border-secondary mt-3">
                            <div class="form-group text-center font-weight-bold">
                                <label for="NameManifestaion">Nom de la manifestation</label>
                                <input type="text" class="form-control" id="NameManifestaion" placeholder="Ecrire Nom" />
                            </div>
                            <div class="form-group text-center font-weight-bold distance-size">
                                <label for="description">Courte description</label>
                                <textarea type="description" rows="5" class="form-control" id="description" placeholder="Ecrire la description" />
                            </div>
                        </form>
                        <div class="custom-file font-weight-bold">
                            <input type="file" class="custom-file-input" id="customFile" />
                            <label class="custom-file-label text-left" for="customFile">Choisir la Photo</label>
                        </div>
                        <div class="form-group space">
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-primary">Envoyer</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}