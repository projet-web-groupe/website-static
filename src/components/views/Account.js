import React, { Component } from 'react';
import { Link } from "react-router-dom";

export default class Account extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div class="container-fluid" id="account">
                <h3> Mon compte :</h3>
                <div class="row justify-content-center" >
                    <div class=" col-lg-3 col-12 text-center " class="account-summary">
                        <a href="#" class="block-account"><strong>Informations personnelles</strong></a><br/><br/>
                        <div class="text-account">Nom</div><br/>
                        <div class="text-account">E-mail</div><br/>
                        <div class="text-account">Numéro de téléphone</div><br/>
                        <div class="text-account">Mot de passe</div><br/>
                        <div class="text-account">Adresse postale</div><br/>
                    </div>
                    <div class=" col-lg-2 col-12 text-center " class="account-summary">
                        <a href="#" class="block-account"><strong>Mes commandes</strong></a><br/><br/>
                        <div class="text-account">Commande n°...</div><br/>
                        <div class="text-account">Commande n°...</div><br/>
                        <div class="text-account">Commande n°...</div><br/>
                    </div>
                    <form>
                        <div class=" col-lg-3 col-12 text-center " class="account-summary">
                            <a href="#" class="block-account"><strong>Modifier mes informations personnelles</strong></a><br/><br/>
                            <div class="form-group">
                                <label for="nom">Votre Nom</label>
                                <input type="text" class="form-control" id="nom" placeholder="Modifier votre nom"/>
                            </div>
                            <div class="form-group">
                                <label for="emailAdress">Email adresse</label>
                                <input type="email" class="form-control" id="emailAdresse" aria-describedby="emailInput" placeholder="ex: exemple@domaine.com"/>
                                <small id="emailInput" class="form-text text-muted">Votre email ne sera jamais partagé</small>
                            </div>
                            <div class="form-group">
                                <label for="phone">Numéro de téléphone</label>
                                <input type="tel" class="form-control" maxlength="10" id="phone" placeholder="ex: 0600000000"/>
                            </div>
                            <div class="form-group">
                                <label for="pwd-customer">Mot de passe</label>
                                <input type="password" class="form-control" id="pwd-customer" placeholder="Modifier votre mot de passe"/>
                            </div>
                            <div class="form-group">
                                <label for="address">Adresse postale</label>
                                <input type="text" class="form-control" id="address" placeholder="Modifier votre adresse"/>
                            </div>

                            <button type="submit" class="btn btn-primary">Confimer vos modification</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}
