import React, { Component } from 'react';
import { Link } from "react-router-dom";

export default class Template extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="container" id="confirm-password">
                <div className="row my-3">
                    <div className="col-12 text-center">
                        <h1>Un email a été envoyé sur votre boite mail :</h1>
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-6">
                        <div className="col-12 text-center my-3">
                            <button type='submit' className="btn btn-primary btn-lg">Renvoyer</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}