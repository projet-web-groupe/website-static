import React from 'react';
import './App.css';
import SideBarLeft from './components/includes/SideBarLeft';
import NavBar from './components/includes/NavBar';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import HomePage from './components/views/HomePage';
import PostActivity from './components/views/PostActivity';
import Store from './components/views/Store';
import Footer from './components/includes/Footer';
import Login from './components/views/Login'
import Register from './components/views/Register'
import SingleActivity from './components/views/SingleActivity';
import Activity from './components/views/Activity';
import Account from './components/views/Account';
import Basket from "./components/views/Basket";
import Password from "./components/views/Password";
import Confirmationpassword from "./components/views/Confirmationpassword";
import Admin from "./components/views/Admin";
import UserManagement from "./components/views/UserManagement";
import StoreManagement from "./components/views/StoreManagement";
import Archive from "./components/views/Archive";
import ActivityManagement from "./components/views/ActivityManagement";


function App() {
    return (
        <BrowserRouter>
            <NavBar/>
            <div class="wrapper">
                <SideBarLeft/>
                <div className="container-fluid p-0 main-container " id="content">
                    <Switch>
                        <Route path="/" exact component={HomePage}/>
                        <Route path="/post-manifestation" component={PostActivity}/>
                        <Route path="/store" component={Store}/>
                        <Route path="/connexion" component={Login}/>
                        <Route path="/inscription" component={Register}/>
                        <Route path="/manifestations" component={Activity}/>
                        <Route path="/single-activity" component={SingleActivity}/>
                        <Route path="/account" component={Account}/>
                        <Route path="/basket" component={Basket}/>
                        <Route path="/password" component={Password}/>
                        <Route path="/confirmation-password" component={Confirmationpassword}/>
                        <Route path="/admin" component={Admin}/>
                        <Route path="/user-management" component={UserManagement}/>
                        <Route path="/store-management" component={StoreManagement}/>
                        <Route path="/archive" component={Archive}/>
                        <Route path="/activity-management" component={ActivityManagement}/>
                    </Switch>
                    <Footer/>
                </div>
            </div>
        </BrowserRouter>
    );
}

export default App;
